#!/bin/sh

# doctl registry kubernetes-manifest > registry-secret.yaml

while read namespace; do
	#kubectl delete secret do-registry --namespace $namespace 2> /dev/null || true
	printf 'namespace %s - ' $namespace
    yq --arg NAMESPACE $namespace -Y \
	   '.metadata.namespace |= "\($NAMESPACE)" | .metadata.name |= "do-registry"' \
	   registry-secret.yaml | kubectl apply -f -
done <<EOF
default
web
sms
EOF
