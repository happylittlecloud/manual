#!/bin/sh

kubectl create secret generic kiali \
		--from-literal=username=kiali \
		--from-literal=passphrase=$(pass kiali-pass) \
		--namespace istio-system 
