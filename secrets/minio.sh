#!/bin/sh

kubectl create secret generic do-spaces-minio \
		--from-literal=AWS_ACCESS_KEY_ID="$(pass minio-key)" \
		--from-literal=AWS_SECRET_ACCESS_KEY="$(pass minio-secret)" \
		--namespace minio

kubectl create secret generic minio-auth \
		--from-literal=MINIO_ACCESS_KEY="$(pass minio-web-key)" \
		--from-literal=MINIO_SECRET_KEY="$(pass minio-web-secret)" \
		--namespace minio
