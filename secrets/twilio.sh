#!/bin/sh

kubectl create secret generic twilio-api \
		--from-literal=TWILIO_SMS=$(pass twilio-sms) \
		--from-literal=TWILIO_SID=$(pass twilio-sid) \
		--from-literal=TWILIO_AUTH_TOKEN=$(pass twilio-auth-token) \
		--from-literal=PHONE=$(pass my-number) \
		--namespace sms
