#!/bin/sh

for namespace in sms web; do
    kubectl create secret generic do-auth-token \
    		--from-literal=DIGITAL_OCEAN_ACCESS_TOKEN=$(pass digital-ocean-read-only) \
    		--namespace $namespace
done
