#!/bin/bash

kubectl create secret generic syncplay-tls \
		$(find ./certs -name '*.pem' | xargs printf " --from-file=%s") \
		--namespace syncplay
