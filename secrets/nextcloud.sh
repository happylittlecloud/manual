#!/bin/sh

kubectl create secret generic nextcloud-auth \
		--from-literal=NEXTCLOUD_ADMIN_PASSWORD=$(pass nextcloud-password) \
		--namespace nextcloud
