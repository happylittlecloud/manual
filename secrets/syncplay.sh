#!/bin/sh

kubectl create secret generic syncplay-pass \
		--from-literal=PASSWORD=$(pass syncplay) \
		--namespace syncplay
