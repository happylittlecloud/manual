#!/bin/sh

kubectl create secret generic maria-db \
		--from-literal=MYSQL_ROOT_PASSWORD=$(pass maria-nextcloud) \
		--from-literal=MYSQL_PASSWORD=$(pass maria-nextcloud-user) \
		--namespace database
